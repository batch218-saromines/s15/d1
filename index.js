console.log("Hello world!");
// enclose with qoutation mark string datas

console. log("Hello world!");
// case sensitive 

console.
log
(
	"Hello, everyone!"
)

// ; delimeter
// we use delimeter to end our code

// [COMMENTS]
// - single line comments
// Shorcut: ctrl + /

// I am a single line comment

/* Multi-line comment */
/*
	I am 
	a 
	multi line
	comment
*/
/* shorcut: ctrl + shift + / */


// Syntax and statement

// Statements in programming are instructions that we tell to computer to perform
// Syntax in programming, it is the set of rules that describes how statements must be considered

// Variables
/*
	it is used to contain data

	-Syntax in declaring variables
	- let/const variableName
*/

		
let myVariable = "Hello";
			//assignment operator (=);
console.log(myVariable);

// console.log(hello); // will result to not defined error

/*
	Guides in writing variables:
		1. Use the 'let' keyword followed by the variable name of your choosing and use the assignment operator (=) to assign a value.
		2. Variable names should start with a lowercase charater, use camelCase for multiple words.
		3. For constant variables, use the 'const' keyword
		4. Variable names shoud be indicative or descriptive of the value being stored.
		5. Never name a varaible starting with numbers
		6. Refrain from using space in declaring a variable

*/

// String
let productName = 'desktop computer';
console.log(productName);

let product = "Alvin's computer"
console.log(product);

// Number
let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;
console.log(interest);

// Reassigning variable value
// Syntax
	// variableName = newValue;

productName = "Laptop";
console.log(productName);

let friend = "Kate";
friend = "Jane";
console.log(friend);

// interest = 4.89;

const pi = 3.14;
// pi = 3.16;
console.log(pi);

// Reassigning - a variable already have a value and we re-assign a new one
// Initialize - it is our first saving of a value

let supplier; // declaration
supplier = "John Smith Tradings"; // initializing
console.log(supplier);

supplier = "Zuitt Store";
console.log(supplier);

// Multiple variable declaration
let productCode = "DC017";
const productBrand = "Dell";
console.log(productCode, productBrand);

// Using a variable with a reserved keyword
//const let = "hello";
//console.log(let);

// [SECTION] Data Types 

// Strings
// Strings are series of characters that create a word, phrase, sentence, or anything related to creating text.
// Strings in Javascript is enclosed with single (' ') or double (" ")qoute
let country = 'Philippines';
let province = "Metro Manila";

console.log(province + ', ' + country);
// We use + symbol to concatenante data / values

let fullAddress = province + ', ' + country;
console.log(fullAddress);

console.log("Philippines" + ', ' + "Metro Manila");

// Escape Character (\)
// "\n" refers to creating a new line or set the text to next line;

console.log("line1\nline2");

let mailAddress = "Metro Manila\nPhilippines";
console.log(mailAddress);

let message = "John's employee went home early.";
console.log(message);

message = 'John\'s employee went home early';
console.log(message);
